ods html close;
ods listing;

title1 'Patient Satisfaction';
data patient;
infile 'S:\stat346\hw7\ch06pr15\ch06pr15.dat';
input satisfaction age severity anxiety;

*1.a;
ods pdf file='S:\stat346\hw7\ch06pr15\scatterplot.pdf';
title2 'Scatterplot of Satisfaction, Age, Severity, and Anxiety';
proc sgscatter data = patient;
	matrix satisfaction age severity anxiety;
run;
ods pdf close;

ods latex file='S:\stat346\hw7\ch06pr15\correlations.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Pairwise correlations';
proc corr data = patient;
	var satisfaction age severity anxiety;
run;
ods latex close;

*1.b;
ods latex file='S:\stat346\hw7\ch06pr15\reg-age-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'MLR with Age and Anxiety';
proc reg data = patient;
	model satisfaction = age anxiety;
	ods select ANOVA;
run;
ods latex close;

ods latex file='S:\stat346\hw7\ch06pr15\reg-age-severity-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'MLR with Age, severity, and Anxiety';
proc reg data = patient;
	model satisfaction = age severity anxiety;
	ods select ANOVA;
run;
ods latex close;

*1.b(2);
ods latex file='S:\stat346\hw7\ch06pr15\glt-age-severity-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'General Linear Test with Age, Severity, and Anxiety';
proc reg data = patient;
	model satisfaction = age severity anxiety;
	TEST1: test severity=0;
	ods select TestANOVA;
run;
ods latex close;

*1.c;
ods latex file='S:\stat346\hw7\ch06pr15\null-hyp-age-severity-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Null Hypothesis Test for Severity and Anxiety';
proc reg data = patient;
	model satisfaction = age severity anxiety;
	TEST1: test severity=0, anxiety=0;
	ods select TestANOVA;
run;
ods latex close;

*2;
ods latex file='S:\stat346\hw7\ch06pr15\predictors-age-severity-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Predictors for MLR';
proc reg data=patient plots=none;
	model satisfaction = age severity anxiety/ss1 ss2 pcorr1 pcorr2 vif;
run;
ods latex close;

*3.a;
ods latex file='S:\stat346\hw7\ch06pr15\glm-age-anxiety.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'General Linear Model';
proc glm data = patient plots=none;
	model satisfaction = age anxiety;
run;
ods latex close;

*3.b;
proc reg data = patient plots=none;
	model satisfaction = age;
	output out = agediag p = pred r = resid;
	
	model satisfaction = age anxiety;
	output out = ageanxietydiag p = pred r = resid;

	model satisfaction = age severity anxiety;
	output out = ageseverityanxietydiag p = pred r = resid;
run;

*3.c;
ods pdf file='S:\stat346\hw7\ch06pr15\age-plots.pdf';
proc gplot data=agediag;
	plot resid*pred/vref=0;
	plot resid*age/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw7\ch06pr15\age-normprob.pdf';
proc capability data=agediag noprint;
	probplot resid;
run;
ods pdf close;

ods pdf file='S:\stat346\hw7\ch06pr15\age-anxiety-plots.pdf';
proc gplot data=ageanxietydiag;
	plot resid*pred/vref=0;
	plot resid*age/vref=0;
	plot resid*anxiety/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw7\ch06pr15\age-anxiety-normprob.pdf';
proc capability data=ageanxietydiag noprint;
	probplot resid;
run;
ods pdf close;

ods pdf file='S:\stat346\hw7\ch06pr15\age-severity-anxiety-plots.pdf';
proc gplot data=ageseverityanxietydiag;
	plot resid*pred/vref=0;
	plot resid*age/vref=0;
	plot resid*severity/vref=0;
	plot resid*anxiety/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw7\ch06pr15\age-severity-anxiety-normprob.pdf';
proc capability data=ageseverityanxietydiag noprint;
	probplot resid;
run;
ods pdf close;

