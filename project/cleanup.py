import csv
import sys

def average(d, k1, k2):
    if d[k1] == '' and d[k2] == '':
        return d[k2]
    elif d[k2] == '':
        return d[k1]
    else:
        return (float(d[k1]) + float(d[k2]))/2.0


if __name__=="__main__":
    lines = []
    fields = []
    with open(sys.argv[1]) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        fields = reader.fieldnames
        for line in reader:
            line['adj_bp.d'] = average(line, 'bp.1d', 'bp.2d')
            line['adj_bp.s'] = average(line, 'bp.1s', 'bp.2s')

            del line['bp.1d']
            del line['bp.1s']
            del line['bp.2d']
            del line['bp.2s']

            for k,v in line.items():
              if v == '':
                line[k] = '-'


            lines.append(line)


    fields.append('adj_bp.s')
    fields.append('adj_bp.d')
    fields.remove('bp.1d')
    fields.remove('bp.1s')
    fields.remove('bp.2d')
    fields.remove('bp.2s')

    with open(sys.argv[2], 'w', newline='') as outfile:
        writer = csv.DictWriter(outfile, fields)
        writer.writeheader()
        writer.writerows(lines)
