*ods html close;
ods listing;


data diabetes;
	infile 'S:\stat346\project\diabetes-clean.csv' dlm=',' firstobs=2 missover dsd;
	input id chol stab_glu hdl ratio glyhb location age gender height weight frame waist hip time_ppn adj_bp_s adj_bp_d;
run;

/*
proc corr data=diabetes noprob;
	var chol hdl ratio glyhb age height weight adj_bp_s adj_bp_d waist hip;
run;

proc reg data = diabetes;
	model glyhb = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=stepwise slentry=0.2 slstay=0.2;
run;

proc reg data = diabetes;
	model glyhb = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=forward slentry=0.2;
run;

proc reg data = diabetes;
	model glyhb = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=backward slstay=0.2;
run;

proc reg data = diabetes;
	model glyhb = chol hdl ratio height weight waist hip adj_bp_s adj_bp_d
		/selection=rsquare adjrsq cp aic sbc b best=2;
run;
*/

proc transreg data = diabetes  plots=(transformation(dependent) obp);
	model BoxCox(glyhb / convenient lambda=-2 to 2 by 0.05) = identity(hdl ratio age height waist time_ppn);
run;

data diabetes2;
	set diabetes;
	inv_glyhb = ((glyhb ** -1.45) - 1)/-1.45;
run;

data diabetes2;
	modify diabetes2;
	if chol>400 then remove;
run;

/*
proc reg data = diabetes2;
	model inv_glyhb = hdl ratio age height waist time_ppn / vif;
run;

proc reg data = diabetes2;
	model inv_glyhb = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=stepwise slentry=0.2 slstay=0.2;
run;
*/

proc reg data = diabetes2;
	model inv_glyhb = chol hdl ratio age height weight waist hip adj_bp_s adj_bp_d 
		/selection=rsquare adjrsq cp aic sbc b best=2;
run;

proc reg data = diabetes2;
	model inv_glyhb = chol hdl ratio age height weight waist hip adj_bp_s adj_bp_d time_ppn
		/selection=rsquare adjrsq cp aic sbc b best=2;
run;
