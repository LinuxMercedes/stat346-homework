*ods html close;
ods listing;

data diabetes;
	infile 'S:\stat346\project\diabetes-clean.csv' dlm=',' firstobs=2;
	input id chol stab_glu hdl ratio glyhb location age gender height weight frame bp_1s bp_1d bp_2s bp_2d waist hip time_ppn adj_bp_s adj_bp_d;
run;

proc corr data=diabetes noprob;
	var chol stab_glu hdl ratio age height weight adj_bp_s adj_bp_d waist hip;
run;

proc reg data = diabetes;
	model stab_glu = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=stepwise slentry=0.2 slstay=0.2;
run;

proc reg data = diabetes;
model stab_glu = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=forward slentry=0.2;
run;

proc reg data = diabetes;
model stab_glu = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=backward slstay=0.2;
run;

proc reg data = diabetes;
model stab_glu = chol hdl ratio height weight waist hip  adj_bp_s adj_bp_d
		/selection=rsquare adjrsq cp aic sbc b best=2;
run;
