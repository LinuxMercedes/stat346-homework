ods html close;
ods listing;

title1 'Muscle Mass';
data mm1;
infile 'S:\stat346\hw1\ch1e27\ch1e27.dat';
input mm age;

ods pdf file='S:\stat346\hw1\ch1e27\regression-plot.pdf';
symbol1 v=circle i=rl;
title2 'Scatter plot of Muscle Mass vs. Age with Regression Line';
proc gplot data = mm1;
plot mm*age/ haxis=axis1 vaxis=axis2;
run;
ods pdf close;

ods latex file='S:\stat346\hw1\ch1e27\regression.tex';
proc reg data =  mm1;
	model mm=age/clb p r;
	output p=pred residual=resid;
	id age;
run;
ods latex close;
