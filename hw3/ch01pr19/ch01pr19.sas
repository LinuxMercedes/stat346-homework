ods html close;
ods listing;


data gpaact1;
infile 'S:\stat346\hw3\ch01pr19\ch01pr19.dat';
input gpa act;

data gpaact2;
infile 'S:\stat346\hw3\ch01pr19\ch01pr19-2.dat';
input gpa act;

/* Sort data by ACT score */
proc sort data=gpaact1 out=byact1;
	by act;
run;

proc sort data=gpaact2 out=byact2;
	by act;
run;

title1 'GPA and ACT';
/*
Create a regression for the data 
Produce confidence limits, predicted values, and residual analysis
Perform significance f-test on the slope (H0: b1 = 0)
*/
*ods latex file='S:\stat346\hw2\ch01pr19\regression.tex';
proc reg data = byact1;
	model gpa=act/ alpha=0.05;
	output p=pred residual=resid out=diag1;
run;


ods pdf file='S:\stat346\hw3\ch01pr19\residuals1.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. time';
proc gplot data=diag1;
	plot resid*act /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch01pr19\normal-probability1.pdf';
title2 'Normal Probability';
proc capability data=diag1 noprint;
	probplot resid;
run;
ods pdf close;

title1 'GPA and ACT with typo';

proc reg data = byact2;
	model gpa=act/ alpha=0.05;
	output p=pred residual=resid out=diag2;
run;
*ods latex close;


ods pdf file='S:\stat346\hw3\ch01pr19\residuals2.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. time';
proc gplot data=diag2;
	plot resid*act /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch01pr19\normal-probability2.pdf';
title2 'Normal Probability';
proc capability data=diag2 noprint;
	probplot resid;
run;
ods pdf close;
