ods html close;
ods listing;

title1 'Plastic Hardness';
data plastic;
infile 'S:\stat346\hw2\ch01pr22\ch01pr22.dat';
input hours hardness;

/* Sort data by ACT score */
proc sort data=plastic out=byhours;
	by hardness;
run;

/*
Create a regression for the data 
Produce confidence limits, predicted values, and residual analysis
Perform significance f-test on the slope (H0: b1 = 0)
*/
* ods latex file='S:\stat346\hw2\ch01pr22\regression.tex';
proc reg data = byhours;
	model hardness=hours/clb clm alpha=0.05;
	output out=diag p=pred r=resid;
run;
* ods latex close;

ods pdf file='S:\stat346\hw3\ch01pr22\residuals.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. time';
proc gplot data=diag;
	plot resid*hours /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch01pr22\normal-probability.pdf';
title2 'Normal Probability';
proc capability data=diag noprint;
	probplot resid;
run;
ods pdf close;


