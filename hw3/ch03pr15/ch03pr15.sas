ods html close;
ods listing;

title1 'Solution Concentration';
data solutionraw;
infile 'S:\stat346\hw3\ch03pr15\ch03pr15.dat';
input concentration time;

proc sort data=solutionraw out=solution;
	by time;
run;

* a;
ods pdf file='S:\stat346\hw3\ch03pr15\scatterplot.pdf';
title2 'Scatterplot Concentration vs. Time';
symbol1 v=circle i=none;
proc gplot data = solution;
plot concentration*time/ haxis=axis1 vaxis=axis2;
run;
ods pdf close;

* b;
* TODO BROKEN need box plot;
ods pdf file='S:\stat346\hw3\ch03pr15\histogram.pdf';
title2 'Histogram';
proc univariate data = solution normal noprint;
	var concentration;
	histogram;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch03pr15\boxplot.pdf';
title2 'Box Plot';
proc boxplot data = solution;
	plot concentration*time;
run;
ods pdf close;

*c;
ods latex file='S:\stat346\hw3\ch03pr15\regression.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Regression';
proc reg data = solution;
	model concentration=time/clb alpha=0.05;
	output out=diag p=pred r=resid;
run;
ods latex close;

*d;
ods pdf file='S:\stat346\hw3\ch03pr15\residuals.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. time';
proc gplot data=diag;
	plot resid*time /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch03pr15\normal-probability.pdf';
title2 'Normal Probability';
proc capability data=diag noprint;
	probplot resid;
run;
ods pdf close;

*e;
ods latex file='S:\stat346\hw3\ch03pr15\lackfit.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Lack-Of-Fit';
proc rsreg data=solution;
	model concentration=time / lackfit covar=1;
run;
ods latex close;

*f;
ods latex file='S:\stat346\hw3\ch03pr15\boxcox.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
ods graphics off;
title2 'Box-Cox Analysis';
proc transreg data=solution;
	model boxcox(time)=identity(concentration);
run;
ods latex close;

data solution2;
set solution;
transconcentration=concentration**(-0.25);
run;

title1 'Solution Concentration ** -0.25'; *transformation;

ods pdf file='S:\stat346\hw3\ch03pr15\scatterplot-trans.pdf';
title2 'Scatterplot Concentration vs. Time'; * transformation;
symbol1 v=circle i=none;
proc gplot data = solution2;
plot transconcentration*time/ haxis=axis1 vaxis=axis2;
run;
ods pdf close;

*g;
ods latex file='S:\stat346\hw3\ch03pr15\regression-trans.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Regression';
proc reg data = solution2;
	model transconcentration=time/clb alpha=0.05;
	output out=diag2 p=pred r=resid;
run;
ods latex close;

*h;
ods pdf file='S:\stat346\hw3\ch03pr15\residuals-trans.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. time';
proc gplot data=diag2;
	plot resid*time /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw3\ch03pr15\normal-probability-trans.pdf';
title2 'Normal Probability';
proc capability data=diag2 noprint;
	probplot resid;
run;
ods pdf close;

*i;
ods latex file='S:\stat346\hw3\ch03pr15\lackfit-trans.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Lack-Of-Fit Test'; 
proc rsreg data=solution2;
	model transconcentration=time / lackfit covar=1;
run;
ods latex close;
