#!/bin/bash

WEBDIR="$HOME/src/school-website"
CLASS='stat346'

IFS=$'\n'


for dir in `ls`
do
	if [ -d "$dir" ] && [ -e "$dir/$dir.tex" ]; then
		pushd "$dir"
		pdflatex "$dir.tex" && pdflatex "$dir.tex"

		mkdir -p "$WEBDIR/homework/$CLASS/"
		cp "$dir.pdf" "$WEBDIR/homework/$CLASS/"
		
		pushd "$WEBDIR/homework/$CLASS"
		git add "$dir.pdf"
		popd

		popd
	fi
done

pushd "$WEBDIR/homework/$CLASS"
git commit -am "Updating $CLASS homework"
git push
popd

