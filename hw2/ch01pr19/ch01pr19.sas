*ods html close;
*ods listing;

title1 'GPA and ACT';
data gpaact1;
infile 'S:\stat346\hw2\ch01pr19\ch01pr19.dat';
input gpa act;

/* Sort data by ACT score */
proc sort data=gpaact1 out=byact;
	by act;
run;

proc print data=byact; 
run;

/* Create a scatter plot
fit a smoothed line with N=80 to it
*/
ods pdf file='S:\stat346\hw2\ch01pr19\scatterplot.pdf';
symbol1 v=circle i=sm80;
title2 'Scatter plot of ACT Score vs. GPA with Smoothing Line N=80';
proc gplot data = byact;
plot gpa*act/ haxis='ACT Score' vaxis='GPA';
run;
ods pdf close;

/*
Create a regression for the data 
Produce confidence limits, predicted values, and residual analysis
Perform significance f-test on the slope (H0: b1 = 0)
*/
ods latex file='S:\stat346\hw2\ch01pr19\regression.tex';
proc reg data = byact;
	model gpa=act/clb clm cli p r alpha=0.05;
	id act;
	output p=pred residual=resid out=diag;
run;
ods latex close;

proc corr data=byact;
	var gpa act;
run;


