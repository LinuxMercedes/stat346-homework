ods html close;
ods listing;

title1 'Plastic Hardness';
data plastic;
infile 'S:\stat346\hw2\ch01pr22\ch01pr22.dat';
input hours hardness;

/* Sort data by ACT score */
proc sort data=plastic out=byhours;
	by hardness;
run;

/* 
Create a scatter plot
and fit a regression line
*/
ods pdf file='S:\stat346\hw2\ch01pr22\scatterplot.pdf';
symbol1 v=circle i=rl;
title2 'Scatter plot of hardness vs time with regression line';
proc gplot data = byhours;
plot hardness*hours/ haxis='Time Elapsed' vaxis='Hardness';
run;
ods pdf close;

/*
Create a regression for the data 
Produce confidence limits, predicted values, and residual analysis
Perform significance f-test on the slope (H0: b1 = 0)
*/
ods latex file='S:\stat346\hw2\ch01pr22\regression.tex';
proc reg data = byhours;
	model hardness=hours/clb clm cli p r alpha=0.05;
	output=diag;
run;
ods latex close;

ods pdf file='S:\stat346\hw2\ch01pr22\confidence-bands.pdf';
symbol1 v=dot c=blue i=none;
symbol2 v=none c=green i=rlclm95;
symbol3 v=none c=red i=rlcli95;
title2 'Confidence and Prediction Bands';
proc gplot data=byhours;
	plot hardness*(hours hours hours)/ overlay;
run;
ods pdf close;

proc corr data=byhours;
	var hardness hours;
run;


