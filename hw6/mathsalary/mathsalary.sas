ods html close;
ods listing;

title1 'Math Salaries';

*a;
data math;
	infile 'S:\stat346\hw6\mathsalary\mathsalary.csv' dlm=',' firstobs=2;
	input salary quality experience publications;
run;

ods pdf file='S:\stat346\hw6\mathsalary\sgscatter.pdf';
title2 'Scatterplot Matrix';
proc sgscatter data=math;
	matrix salary quality experience publications;
run;
ods pdf close;

ods latex file='S:\stat346\hw6\mathsalary\correlations.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Correlation Matrix';
proc corr data=math noprob;
	var salary quality experience publications;
run;
ods latex close;

*b;
ods latex file='S:\stat346\hw6\mathsalary\simplereg.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Simple Regression';
proc reg data=math;
	model salary = quality;
	model salary = experience;
	model salary = publications;
run;
ods latex close;

*c;
ods latex file='S:\stat346\hw6\mathsalary\multiplereg.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Multiple Regression';
proc reg data=math;
	model salary = quality experience publications / clb alpha=0.01666;
	output out = diag p = pred r = resid;
run;
ods latex close;

*g;
data mathpred;
	quality=5.4;
	experience=17;
	publications=6.0;
	output;
run;

data math2; 
	set math mathpred;
run;

ods latex file='S:\stat346\hw6\mathsalary\multipleregpred.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Multiple Regression with Prediction';
proc reg data=math2;
	model salary = quality experience publications / cli clm alpha=0.05;
run;
ods latex close;

*k;

ods pdf file='S:\stat346\hw6\mathsalary\residuals-pred.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. Predicted Salary';
proc gplot data=diag;
	plot resid*pred /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw6\mathsalary\residuals-quality.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. Quality';
proc gplot data=diag;
	plot resid*quality /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw6\mathsalary\residuals-experience.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. Experience';
proc gplot data=diag;
	plot resid*experience /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw6\mathsalary\residuals-publications.pdf';
symbol1 v=dot i=none;
title2 'Residuals vs. Publications';
proc gplot data=diag;
	plot resid*publications /vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw6\mathsalary\normal-probability.pdf';
title2 'Normal Probability';
proc capability data=diag noprint;
	probplot resid;
run;
ods pdf close;
