ods html close;
ods listing;

title1 'Punting';
data football;
	infile 'S:\stat346\hw8\football\football_full.csv' dlm=',' firstobs=2;
	input distance hang strr strl flexr flexl strt;
run;

*3.a;
ods pdf file='S:\stat346\hw8\football\scatterplot.pdf';
title2 'Scatterplot';
proc sgscatter data = football;
	matrix distance hang strr strl flexr flexl strt;
run;
ods pdf close;

ods latex file='S:\stat346\hw8\football\correlations.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Correlation Matrix';
proc corr data=football noprob;
	var distance hang strr strl flexr flexl strt;
run;
ods latex close;


*3.b;
ods latex file='S:\stat346\hw8\football\reg.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Best 2 MLR models';
proc reg data = football;
	model distance = hang strr strl flexr flexl strt
		/selection=rsquare adjrsq cp aic sbc b best=2;
run;
ods latex close;

*3.c;
ods latex file='S:\stat346\hw8\football\fsel.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Forward Selection MLR';
proc reg data = football;
	model distance = hang strr strl flexr flexl strt
		/selection=forward slentry=0.2;
run;
ods latex close;

ods latex file='S:\stat346\hw8\football\bsel.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Backward Selection MLR';
proc reg data = football;
	model distance = hang strr strl flexr flexl strt
		/selection=backward slstay=0.2;
run;
ods latex close;

ods latex file='S:\stat346\hw8\football\ssel.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Stepwise Selection MLR';
proc reg data = football;
	model distance = hang strr strl flexr flexl strt
		/selection=stepwise slentry=0.2 slstay=0.2;
run;
ods latex close;


*3.d;
* using model with strt and strr;
ods latex file='S:\stat346\hw8\football\best.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'MLR with Right Leg and Total Leg Strength';
proc reg data = football plots=none;
	model distance = strt strr;
	output out = diag p = pred r = resid;
run;
ods latex close;

ods pdf file='S:\stat346\hw8\football\predictor.pdf';
title2 'Residuals vs. Predicted';
proc gplot data=diag;
	plot resid*pred/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw8\football\strt.pdf';
title2 'Residuals vs. Total Leg Strength';
proc gplot data=diag;
	plot resid*strt/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw8\football\strl.pdf';
title2 'Residuals vs. Right Leg Strength';
proc gplot data=diag;
	plot resid*strl/vref=0;
run;
ods pdf close;

ods pdf file='S:\stat346\hw8\football\normprob.pdf';
proc capability data=diag noprint;
	probplot resid;
run;
ods pdf close;
