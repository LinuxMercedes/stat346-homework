ods html close;
ods listing;

title1 'Body Fat';
data fat;
	infile 'S:\stat346\hw8\bodyfat\BodyFat.csv' dlm=',' firstobs=2;
	input triceps thigh midarm perbodyfat;
run;

*2.a;
ods pdf file='S:\stat346\hw8\bodyfat\scatterplot.pdf';
title2 'Scatterplot';
proc sgscatter data = fat;
	matrix triceps thigh midarm perbodyfat;
run;
ods pdf close;

ods latex file='S:\stat346\hw8\bodyfat\correlations.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Pairwise correlations';
proc corr data = fat;
	var triceps thigh midarm perbodyfat;
run;
ods latex close;

*2.b;
ods latex file='S:\stat346\hw8\bodyfat\reg.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Multiple Linear Regression';
proc reg data = fat;
	model perbodyfat = triceps thigh midarm;
run;
ods latex close;

