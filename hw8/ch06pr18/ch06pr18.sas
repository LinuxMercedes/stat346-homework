ods html close;
ods listing;

title1 'Commercial Properties';
data properties;
infile 'S:\stat346\hw8\ch06pr18\ch06pr18.dat';
input rental age operating vacancy sqfootage;

*1.a;
ods latex file='S:\stat346\hw8\ch06pr18\reg.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Multiple Linear Regression';
proc reg data = properties;
	model rental = age operating vacancy sqfootage / stb;
run;
ods latex close;
