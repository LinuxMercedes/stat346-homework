ods html close;
ods listing;

title1 'Bread Brand Liking';
data bread;
infile 'S:\stat346\hw5\ch06pr5\CH06PR05.dat';
input liking moisture sweetness;

* a;
ods pdf file='S:\stat346\hw5\ch06pr5\scatterplot.pdf';
title2 'Scatterplot Moisture, Sweetness vs. Liking';
proc sgscatter data = bread;
	matrix liking moisture sweetness;
run;
ods pdf close;

ods latex file='S:\stat346\hw5\ch06pr5\correlations.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Pairwise correlations';
proc corr data = bread;
	var liking moisture sweetness;
run;
ods latex close;

* b;
ods latex file='S:\stat346\hw5\ch06pr5\moisture-regression.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Moisture Regression';
proc reg data = bread;
	model liking=moisture/clb alpha=0.01;
	output out=diag p=pred r=resid;
run;
ods latex close;

*c;
ods latex file='S:\stat346\hw5\ch06pr5\sweetness-regression.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Sweetness Regression';
proc reg data = bread;
	model liking=sweetness/clb alpha=0.01;
	output out=diag p=pred r=resid;
run;
ods latex close;

*d;
ods latex file='S:\stat346\hw5\ch06pr5\multiple-regression.tex' (notop nobot) stylesheet="sas.sty"(url="sas");
title2 'Multiple Regression';
proc reg data = bread;
	model liking=moisture sweetness/clb alpha=0.01;
	output out=diag p=pred r=resid;
run;
ods latex close;
